#include <stxxl/io>
#include <stxxl/vector>

#include <iostream>
#include <fstream>

typedef long long i64;
typedef int i32;

#pragma pack(push, 1)
struct TItem {
    i64 Vertex;
    i64 Parent;
    i32 Value;
};

static_assert(sizeof(TItem) == 20, "wrong size of TItem");
#pragma pack(pop)


int main(int argc, const char* argv[]) {
    if (argc != 3) {
        std::cerr << "usage: " << argv[0] << " textFile binFile" << std::endl;
        return EXIT_FAILURE;
    }
    
    const char* inputFileName = argv[1];
    const char* outputFileName = argv[2];

    std::ifstream fin(inputFileName);
    
    stxxl::syscall_file outFile(outputFileName, stxxl::file::DIRECT | stxxl::file::WRONLY | stxxl::file::CREAT);
    stxxl::vector<TItem, 1, stxxl::lru_pager<8>, 1024 * sizeof(TItem)> v(&outFile);
    TItem item;
    while (fin >> item.Vertex >> item.Parent >> item.Value) {
        v.push_back(item);
    }
    return 0;
}
