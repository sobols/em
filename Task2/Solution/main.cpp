#include <stxxl/vector>
#include <stxxl/sort>
#include <stxxl/priority_queue>

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <queue>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <vector>

//#define DEBUG

typedef long long i64;
typedef int i32;

const i64 ROOT = 0;

const size_t GB = 1 << 30;
const size_t MAX_RAM_FOR_SORT = 2 * GB;
const size_t MAX_RAM_FOR_QUEUE = 1 * GB;
const size_t MAX_RAM_FOR_LIST_RANKING = 2 * GB;

// Some helper classes

class TClock : public std::ostringstream {
public:
    TClock()
    {
    }
    TClock(const char* msg)
    {
        (*this) << msg;
        Begin();
    }
    void Begin()
    {
        DoPrint("BEG");
    }
    ~TClock()
    {
        DoPrint("END");
    }
private:
    void DoPrint(const char* stage) {
        std::cout << std::fixed << std::setw(6) << std::setfill(' ') <<
            clock() / CLOCKS_PER_SEC << " s: " << stage << " " << this->str() << std::endl;
    }
};

/*
 * STL wrappers
 */
template<class T>
class TStdVector : public std::vector < T > {
};

template<class T, class Cmp>
class TStdPriorityQueue : public std::priority_queue < T, std::vector<T>, Cmp > {
};

template<class T, class C>
void Sort(TStdVector<T>& v, C cmp) {
    std::sort(v.begin(), v.end(), cmp);
}


/*
* STXXL wrappers
*/
template<class T>
class TStxxlVector : public stxxl::vector < T > {
};

template<class T, class Cmp>
using TStxxlPriorityQueueImpl = typename stxxl::PRIORITY_QUEUE_GENERATOR <
    T,
    Cmp,
    128 * 1024 * 1024,
    1024 * 1024
>::result;

template<class T, class Cmp>
class TStxxlPriorityQueue : public TStxxlPriorityQueueImpl<T, Cmp> {
public:
    TStxxlPriorityQueue() :
        TStxxlPriorityQueueImpl<T, Cmp>(MAX_RAM_FOR_QUEUE, MAX_RAM_FOR_QUEUE)
    {}
};

template<class T, class C>
void Sort(TStxxlVector<T>& v, C cmp) {
    stxxl::sort(v.begin(), v.end(), cmp, MAX_RAM_FOR_SORT);
}

template<class T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type Min()
{
    return std::numeric_limits<T>::min();
}
template<class T>
typename std::enable_if<!std::is_arithmetic<T>::value, T>::type Min()
{
    return T::GetMin();
}
template<class T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type Max()
{
    return std::numeric_limits<T>::max();
}
template<class T>
typename std::enable_if<!std::is_arithmetic<T>::value, T>::type Max()
{
    return T::GetMax();
}
template<class T>
class TStxxlCmp {
public:
    static T min_value() {
        return Min<T>();
    }
    static T max_value() {
        return Max<T>();
    }
};


// Solution starts here

struct TEdge {
    i64 From;
    i64 To;

    TEdge() {}
    TEdge(i64 from, i64 to)
        : From(from)
        , To(to)
    {
    }
    static TEdge GetMin() {
        return TEdge(Min<i64>(), Min<i64>());
    }
    static TEdge GetMax() {
        return TEdge(Max<i64>(), Max<i64>());
    }
};
struct TSortEdgeByFrom : public TStxxlCmp<TEdge> {
    bool operator()(const TEdge& lhs, const TEdge& rhs) const {
        return lhs.From < rhs.From;
    }
};


enum EMark {
    DOWN = +1,
    UP = -1,
};
struct TMarkedEdge {
    i64 From;
    i64 To;
    int Mark;

    TMarkedEdge() {}
    TMarkedEdge(i64 from, i64 to, int mark)
        : From(from)
        , To(to)
        , Mark(mark)
    {
    }
    static TMarkedEdge GetMin() {
        return TMarkedEdge(Min<i64>(), Min<i64>(), UP);
    }
    static TMarkedEdge GetMax() {
        return TMarkedEdge(Max<i64>(), Max<i64>(), DOWN);
    }
};
bool operator <(const TMarkedEdge& lhs, const TMarkedEdge& rhs) {
    if (lhs.From != rhs.From) {
        return lhs.From < rhs.From;
    }
    if (lhs.To != rhs.To) {
        return lhs.To < rhs.To;
    }
    return lhs.Mark < rhs.Mark;
}
bool operator ==(const TMarkedEdge& lhs, const TMarkedEdge& rhs) {
    return lhs.From == rhs.From && lhs.To == rhs.To && lhs.Mark == rhs.Mark;
}
bool operator !=(const TMarkedEdge& lhs, const TMarkedEdge& rhs) {
    return !(lhs == rhs);
}
std::ostream& operator <<(std::ostream& out, const TMarkedEdge& e) {
    return out << "(" << e.From << ", " << e.To << ", " << (e.Mark == DOWN ? "down" : e.Mark == UP ? "up" : "unknown") << ")";
}

typedef TMarkedEdge TElem; // edges are elements for list ranking

struct TVNextV {
    TElem V;
    TElem NextV;

    TVNextV() {}
    TVNextV(TElem v, TElem nextV)
        : V(v)
        , NextV(nextV)
    {
    }

    static TVNextV GetMin() {
        return TVNextV(Min<TElem>(), Min<TElem>());
    }
    static TVNextV GetMax() {
        return TVNextV(Max<TElem>(), Max<TElem>());
    }
};

struct TVWeight {
    TElem V;
    i64 Weight;

    TVWeight() {}
    TVWeight(TElem v, i64 weight)
        : V(v)
        , Weight(weight)
    {
    }

    static TVWeight GetMin() {
        return TVWeight(Min<TElem>(), Min<i64>());
    }
    static TVWeight GetMax() {
        return TVWeight(Max<TElem>(), Max<i64>());
    }
};

struct TVB {
    TElem V;
    bool B;

    TVB() {}
    TVB(TElem v, bool b)
        : V(v)
        , B(b)
    {
    }

    static TVB GetMin() {
        return TVB(Min<TElem>(), false);
    }
    static TVB GetMax() {
        return TVB(Max<TElem>(), true);
    }
};

struct TVD {
    TElem V;
    bool D;

    TVD() {}
    TVD(TElem v, bool d)
        : V(v)
        , D(d)
    {
    }

    static TVD GetMin() {
        return TVD(Min<TElem>(), false);
    }
    static TVD GetMax() {
        return TVD(Max<TElem>(), true);
    }
};

struct TVNextVWeight {
    TElem V;
    TElem NextV;
    i64 Weight;

    TVNextVWeight() {}
    TVNextVWeight(TElem v, TElem nextV, i64 weight)
        : V(v)
        , NextV(nextV)
        , Weight(weight)
    {
    }

    static TVNextVWeight GetMin() {
        return TVNextVWeight(Min<TElem>(), Min<TElem>(), Min<i64>());
    }
    static TVNextVWeight GetMax() {
        return TVNextVWeight(Max<TElem>(), Max<TElem>(), Max<i64>());
    }
};


struct TVNextVWeightB {
    TElem V;
    TElem NextV;
    i64 Weight;
    bool B;

    TVNextVWeightB() {}
    TVNextVWeightB(TElem v, TElem nextV, i64 weight, bool b)
        : V(v)
        , NextV(nextV)
        , Weight(weight)
        , B(b)
    {
    }

    static TVNextVWeightB GetMin() {
        return TVNextVWeightB(Min<TElem>(), Min<TElem>(), Min<i64>(), false);
    }
    static TVNextVWeightB GetMax() {
        return TVNextVWeightB(Max<TElem>(), Max<TElem>(), Max<i64>(), true);
    }
};

struct TVNextVWeightD {
    TElem V;
    TElem NextV;
    i64 Weight;
    bool D;

    TVNextVWeightD() {}
    TVNextVWeightD(TElem v, TElem nextV, i64 weight, bool d)
        : V(v)
        , NextV(nextV)
        , Weight(weight)
        , D(d)
    {
    }

    static TVNextVWeightD GetMin() {
        return TVNextVWeightD(Min<TElem>(), Min<TElem>(), Min<i64>(), false);
    }
    static TVNextVWeightD GetMax() {
        return TVNextVWeightD(Max<TElem>(), Max<TElem>(), Max<i64>(), true);
    }
};

struct TVRank {
    TElem V;
    i64 Rank;

    TVRank() {}
    TVRank(TElem v, i64 rank)
        : V(v)
        , Rank(rank)
    {
    }

    static TVRank GetMin() {
        return TVRank(Min<TElem>(), Min<i64>());
    }
    static TVRank GetMax() {
        return TVRank(Max<TElem>(), Max<i64>());
    }
};

struct TSortByRank : public TStxxlCmp<TVRank> {
    bool operator()(const TVRank& lhs, const TVRank& rhs) const {
        return lhs.Rank < rhs.Rank;
    }
};

bool RandomBool() {
    return (rand() & (1 << 7)) != 0;
}

template<class T>
struct TSortByV : public TStxxlCmp<T> {
    bool operator()(const T& lhs, const T& rhs) const {
        return lhs.V < rhs.V;
    }
};
template<class T>
struct TSortByNextV : public TStxxlCmp<T> {
    bool operator()(const T& lhs, const T& rhs) const {
        return lhs.NextV < rhs.NextV;
    }
};

// for RAM vector only!
size_t FindBS(const std::vector<TElem>& vs, const TElem what) {
    const size_t j = std::lower_bound(vs.begin(), vs.end(), what) - vs.begin();
    assert(j < vs.size() && vs[j] == what);
    return j;
}

template<template<class> class TVector>
void DoListRankingInMemory(const TVector<TVNextV>& vNextV, const TVector<TVWeight>& vWeight, TVector<TVRank>* out)
{
    TClock clk;
    clk << "List Ranking in RAM (" << vNextV.size() << " elements)";
    clk.Begin();

    // RAM
    std::vector<TElem> vs;
    std::vector<TElem> nextVs;
    std::vector<i64> weights;

    vs.reserve(vNextV.size());
    for (auto it = vNextV.begin(); it != vNextV.end(); ++it) {
        vs.push_back(it->V);
    }

    std::sort(vs.begin(), vs.end());
    for (size_t i = 0; i + 1 < vs.size(); ++i) {
        assert(vs[i] < vs[i + 1]); // no equal keys
    }

    nextVs.resize(vs.size());

    for (auto it = vNextV.begin(); it != vNextV.end(); ++it) {
        nextVs[FindBS(vs, it->V)] = it->NextV;
    }
    weights.resize(vs.size());
    for (auto it = vWeight.begin(); it != vWeight.end(); ++it) {
        weights[FindBS(vs, it->V)] = it->Weight;
    }

    i64 sumRank = 0;

    const TElem start = vNextV.front().V;
    TElem cur = start;

    do {
        out->push_back(TVRank(cur, sumRank));
        const size_t j = FindBS(vs, cur);
        sumRank += weights[j];
        cur = nextVs[j];
    } while (cur != start);

    assert(out->size() == vNextV.size());
}

template<template<class> class TVector>
void DoListRanking(TVector<TVNextV> vNextV, TVector<TVWeight> vWeight, TVector<TVRank>* out)
{
    if (vNextV.size() * (sizeof(TElem) + sizeof(TElem) + sizeof(i64)) <= MAX_RAM_FOR_LIST_RANKING) {
        DoListRankingInMemory(vNextV, vWeight, out);
        return;
    }

    TClock clk;
    clk << "List Ranking in EM (" << vNextV.size() << " elements)";
    clk.Begin();

    TVector<TVB> vB;
    TVector<TVNextVWeightB> vNextVWeightB;

    Sort(vNextV, TSortByV<TVNextV>());
    Sort(vWeight, TSortByV<TVWeight>());

    {
        auto it = vNextV.begin();
        auto jt = vWeight.begin();

        while (it != vNextV.end()) {
            assert(it->V == jt->V);

            const bool curB = RandomBool();

            vB.push_back(TVB(it->V, curB));

            // b(next(v)) is not known yet
            vNextVWeightB.push_back(TVNextVWeightB(it->V, it->NextV, jt->Weight, curB));

            ++it;
            ++jt;
        }
        assert(jt == vWeight.end());
    }

    Sort(vB, TSortByV<TVB>());
    Sort(vNextVWeightB, TSortByNextV<TVNextVWeightB>());

    TVector<TVD> vD;
    TVector<TVNextVWeightD> vNextVWeightD;
    {
        auto it = vB.begin();
        auto jt = vNextVWeightB.begin();

        while (it != vB.end()) {
            assert(it->V == jt->NextV);

            const TElem v = jt->V;
            const i64 weight = jt->Weight;
            const TElem nextV = jt->NextV;

            const bool b = jt->B;
            const bool nextB = it->B;

            const bool d = (b && !nextB);
            vD.push_back(TVD(v, d));
            vNextVWeightD.push_back(TVNextVWeightD(v, nextV, weight, d));

            ++it;
            ++jt;
        }
        assert(jt == vNextVWeightB.end());
    }

    TVector<TVNextV> subtaskVNextV;
    TVector<TVNextVWeight> vRemovedVWeight;
    TVector<TVWeight> subtaskVWeight;

    Sort(vD, TSortByV<TVD>());
    {
        auto it = vD.begin();
        auto jt = vNextV.begin();
        auto kt = vNextVWeightD.begin();
        auto pt = vWeight.begin();

        while (it != vD.end()) {

            assert(it->V == jt->V && jt->V == kt->NextV && kt->NextV == pt->V);

            const TElem v = kt->V;
            const TElem nextV = kt->NextV;
            const TElem nextNextV = jt->NextV;

            const bool d = kt->D;
            const bool nextD = it->D;

            const i64 weight = kt->Weight;
            const i64 nextWeight = pt->Weight;

#ifdef DEBUG
            std::cerr << v << " -> " << nextV << " -> " << nextNextV << " (deletes " << d << ", " << nextD << ") (weights " << weight << ", " << nextWeight << ")\n";
#endif

            if (nextD) {
                subtaskVNextV.push_back(TVNextV(v, nextNextV));
                subtaskVWeight.push_back(TVWeight(v, weight + nextWeight));
                vRemovedVWeight.push_back(TVNextVWeight(v, nextV, weight));
            } else if (!d && !nextD) {
                subtaskVNextV.push_back(TVNextV(v, nextV));
                subtaskVWeight.push_back(TVWeight(v, weight));
            }

            ++it;
            ++jt;
            ++kt;
            ++pt;
        }
        assert(jt == vNextV.end());
        assert(kt == vNextVWeightD.end());
        assert(pt == vWeight.end());
    }

    TVector<TVRank> subtaskOut;
    DoListRanking(subtaskVNextV, subtaskVWeight, &subtaskOut);

    Sort(vRemovedVWeight, TSortByV<TVNextVWeight>());
    Sort(subtaskOut, TSortByV<TVRank>());
    {
        auto jt = vRemovedVWeight.begin();
        for (auto it = subtaskOut.begin(); it != subtaskOut.end(); ++it) {
            out->push_back(*it);
            while (jt != vRemovedVWeight.end() && jt->V < it->V) {
                ++jt;
            }
            if (jt != vRemovedVWeight.end() && jt->V == it->V) {
#ifdef DEBUG
                std::cerr << "putting " << jt->NextV << " rank " << it->Rank << " plus " << jt->Weight << '\n';
#endif
                out->push_back(TVRank(jt->NextV, it->Rank + jt->Weight));
            }
        }
    }
}

template<template<class> class TVector>
void ListRanking(TVector<TVNextV> vNextV, TVector<TVRank>* out)
{
    TClock clk("Main List Ranking function");
    TVector<TVWeight> vWeight;
    vWeight.reserve(vNextV.size());
    for (auto it = vNextV.begin(); it != vNextV.end(); ++it) {
        vWeight.push_back(TVWeight(it->V, 1));
    }
    out->clear();
    out->reserve(vNextV.size());
    DoListRanking(vNextV, vWeight, out);
}

template<template<class> class TVector>
void PrepareDataForListRanking(const TVector<TEdge>& edges, const TVector<TEdge>& backEdges, TVector<TVNextV>* edgeOrder)
{
    TClock clk("Prepare for List Ranking");

    auto it = edges.begin();
    auto jt = backEdges.begin();

    while (!(it == edges.end() && jt == backEdges.end())) {
        if (it != edges.end() && (jt == backEdges.end() || jt->From > it->From)) {
            // root
            const i64 v = it->From;
#ifdef DEBUG
            std::cerr << "=== root " << v << " ===\n";
#endif

            bool first = true;
            i64 firstSon = 0;
            i64 prevSon = 0;

            while (it != edges.end() && it->From == v) {
#ifdef DEBUG
                std::cerr << "edge to " << it->To << '\n';
#endif
                if (first) {
                    firstSon = it->To;
                    first = false;
                } else {
                    edgeOrder->push_back(TVNextV(TMarkedEdge(prevSon, v, UP), TMarkedEdge(v, it->To, DOWN)));
                }
                prevSon = it->To;
                ++it;
            }
            edgeOrder->push_back(TVNextV(TMarkedEdge(prevSon, v, UP), TMarkedEdge(v, firstSon, DOWN)));

        } else if (it != edges.end() && (jt != backEdges.end() && jt->From == it->From)) {
            // intermediate vertex
            const i64 v = it->From;
#ifdef DEBUG
            std::cerr << "=== vertex " << v << " ===\n";
#endif
            const i64 parent = jt->To;
#ifdef DEBUG
            std::cerr << "its parent is " << parent << '\n';
#endif
            ++jt;
            assert(jt == backEdges.end() || jt->From > v);

            bool first = true;
            i64 prevSon = 0;

            while (it != edges.end() && it->From == v) {
                const i64 son = it->To;
#ifdef DEBUG
                std::cerr << "edge to " << son << '\n';
#endif
                if (first) {
                    edgeOrder->push_back(TVNextV(TMarkedEdge(parent, v, DOWN), TMarkedEdge(v, son, DOWN)));
                    first = false;
                } else {
                    edgeOrder->push_back(TVNextV(TMarkedEdge(prevSon, v, UP), TMarkedEdge(v, son, DOWN)));
                }
                prevSon = son;
                ++it;
            }
            edgeOrder->push_back(TVNextV(TMarkedEdge(prevSon, v, UP), TMarkedEdge(v, parent, UP)));

        } else if (jt != backEdges.end() && (it == edges.end() || it->From > jt->From)) {
            // leaf
            const i64 v = jt->From;
#ifdef DEBUG
            std::cerr << "=== leaf " << v << " ===\n";
#endif
            const i64 parent = jt->To;
#ifdef DEBUG
            std::cerr << "its parent is " << parent << '\n';
#endif
            ++jt;
            assert(jt == backEdges.end() || jt->From > v);

            edgeOrder->push_back(TVNextV(TMarkedEdge(parent, v, DOWN), TMarkedEdge(v, parent, UP)));

        } else {
            assert(false);
        }
    }
}

struct TRenumber {
    i64 OldNumber;
    i64 NewNumber;

    TRenumber() {}
    TRenumber(i64 oldNumber, i64 newNumber)
        : OldNumber(oldNumber)
        , NewNumber(newNumber)
    {
    }

    static TRenumber GetMin() {
        return TRenumber(Min<i64>(), Min<i64>());
    }
    static TRenumber GetMax() {
        return TRenumber(Max<i64>(), Max<i64>());
    }
};
struct TSortByOldNumber : public TStxxlCmp<TRenumber> {
    bool operator()(const TRenumber& lhs, const TRenumber& rhs) const {
        return lhs.OldNumber < rhs.OldNumber;
    }
};

template<template<class> class TVector>
void RenumberVertices(const TVector<TVRank>& ranks, TVector<TRenumber>* renumbers)
{
    TClock clk("Renumber vertices");
    bool active = false;
    i64 firstToGo = 0;
    i64 counter = 0;

    for (auto it = ranks.begin(); it != ranks.end(); ++it) {
        if (!active) {
            if (it->V.From == ROOT && it->V.Mark == DOWN) {
                // we leave the root
                firstToGo = it->V.To;
                active = true;
            }
        } else {
            if (it->V.Mark == UP) {
                renumbers->push_back(TRenumber(it->V.From, counter));
                counter++;
            }
        }
    }
    assert(active);

    for (auto it = ranks.begin(); it != ranks.end(); ++it) {
        if (it->V.From == ROOT && it->V.To == firstToGo && it->V.Mark == DOWN) {
            active = false;
            break;
        } else {
            if (it->V.Mark == UP) {
                renumbers->push_back(TRenumber(it->V.From, counter));
                counter++;
            }
        }
    }
    assert(!active);

    // finally renumber the root
    renumbers->push_back(TRenumber(ROOT, counter));
}

struct TValues {
    i64 Vertex;
    i64 PrevVertex;

    i64 NewVertex;
    i64 NewPrevVertex;

    i32 Value;

    TValues() {}
    TValues(i64 vertex, i64 prevVertex, i32 value)
        : Vertex(vertex)
        , PrevVertex(prevVertex)
        , NewVertex(0)
        , NewPrevVertex(0)
        , Value(value)
    {
    }

    TValues(i64 vertex, i64 prevVertex, i64 newVertex, i64 newPrevVertex, i32 value)
        : Vertex(vertex)
        , PrevVertex(prevVertex)
        , NewVertex(newVertex)
        , NewPrevVertex(newPrevVertex)
        , Value(value)
    {
    }

    static TValues GetMin() {
        return TValues(Min<i64>(), Min<i64>(), Min<i64>(), Min<i64>(), Min<i32>());
    }
    static TValues GetMax() {
        return TValues(Max<i64>(), Max<i64>(), Max<i64>(), Max<i64>(), Max<i32>());
    }
};
struct TSortByVertex : public TStxxlCmp<TValues> {
    bool operator()(const TValues& lhs, const TValues& rhs) const {
        return lhs.Vertex < rhs.Vertex;
    }
};
struct TSortByNewVertex : public TStxxlCmp<TValues> {
    bool operator()(const TValues& lhs, const TValues& rhs) const {
        return lhs.NewVertex < rhs.NewVertex;
    }
};
struct TSortByPrevVertex : public TStxxlCmp<TValues> {
    bool operator()(const TValues& lhs, const TValues& rhs) const {
        return lhs.PrevVertex < rhs.PrevVertex;
    }
};

template<template<class> class TVector>
void JoinNewNumbers(TVector<TValues>* values, const TVector<TRenumber>& renumbers)
{
    TClock clk("Join new vertex numbers");
    Sort(*values, TSortByPrevVertex());
    {
        auto jt = renumbers.begin();
        for (auto it = values->begin(); it != values->end(); ++it) {
            while (jt != renumbers.end() && jt->OldNumber < it->PrevVertex) {
                ++jt;
            }
            assert(jt != renumbers.end() && jt->OldNumber == it->PrevVertex);

            it->NewPrevVertex = jt->NewNumber;
        }
    }

    Sort(*values, TSortByVertex());
    {
        auto jt = renumbers.begin();
        for (auto it = values->begin(); it != values->end(); ++it) {
            while (jt != renumbers.end() && jt->OldNumber < it->Vertex) {
                ++jt;
            }
            assert(jt != renumbers.end() && jt->OldNumber == it->Vertex);

            it->NewVertex = jt->NewNumber;
        }
    }
}


struct TTFPItem {
    i64 Vertex;
    double KnownScore;

    TTFPItem() {}
    TTFPItem(i64 vertex, double knownScore)
        : Vertex(vertex)
        , KnownScore(knownScore)
    {
    }
};

std::ostream& operator <<(std::ostream& out, const TTFPItem& item) {
    return out << "(vertex " << item.Vertex << ", value " << item.KnownScore << ")";
}

struct TTFPCmp {
    bool operator()(const TTFPItem& lhs, const TTFPItem& rhs) const {
        return lhs.Vertex > rhs.Vertex;
    }
    static TTFPItem min_value() {
        return TTFPItem(Max<i64>(), 0.0);
    }
    static TTFPItem max_value() {
        return TTFPItem(Min<i64>(), 0.0);
    }
};

template<template<class> class TVector, template<class, class> class TPriorityQueue>
double RunTimeForwardProcessing(const TVector<TValues>& values)
{
    TClock clk("Time-forward Procesing");
    TPriorityQueue<TTFPItem, TTFPCmp> q;

    for (auto it = values.begin(); it != values.end(); ++it) {
        const i64 v = it->NewVertex;

        double incomingSum = 0;
        i64 incomingCount = 0;

        while (!q.empty() && q.top().Vertex == v) {
            incomingSum += q.top().KnownScore;
            incomingCount++;
            q.pop();
        }
        if (incomingCount == 0) {
            incomingCount = 1;
            incomingSum = it->Value;
        }

        const double curValue = incomingSum / incomingCount;

#ifdef DEBUG
        std::cerr << "send from " << it->NewVertex << " (old number " << it->Vertex << ") to " << it->NewPrevVertex << " (old number " << it->PrevVertex << ") a value " << curValue << '\n';
#endif

        q.push(TTFPItem(it->NewPrevVertex, curValue));
    }

    // process the root
    double incomingSum = 0;
    i64 incomingCount = 0;

    while (!q.empty()) {
        incomingSum += q.top().KnownScore;
        incomingCount++;
        q.pop();
    }
    assert(incomingCount != 0);
    return incomingSum / incomingCount;
}

class ISolver {
public:
    virtual void Add(i64 vertex, i64 parent, i32 value) = 0;
    virtual double Solve() = 0;
};

template<template<class> class TVector, template<class, class> class TPriorityQueue>
class TSolver : public ISolver {
public:
    void Add(i64 vertex, i64 parent, i32 value) {
        edges.push_back(TEdge(parent, vertex));
        backEdges.push_back(TEdge(vertex, parent));
        values.push_back(TValues(vertex, parent, value));
    }
    double Solve() {
        srand(1);
        Sort(edges, TSortEdgeByFrom());
        Sort(backEdges, TSortEdgeByFrom());

        TVector<TVNextV> edgeOrder; // for list ranking
        PrepareDataForListRanking(edges, backEdges, &edgeOrder);

#ifdef DEBUG
        std::cerr << "Data for list ranking:\n";
        for (auto it : edgeOrder) {
            std::cerr << it.V << " => " << it.NextV << '\n';
        }
#endif

        TVector<TVRank> ranks;
        ListRanking(edgeOrder, &ranks);
        Sort(ranks, TSortByRank());

#ifdef DEBUG
        std::cerr << "Data after list ranking:\n";
        for (auto it : ranks) {
            std::cerr << it.V << " has rank " << it.Rank << '\n';
        }
#endif


        TVector<TRenumber> renumbers;
        RenumberVertices(ranks, &renumbers);
        Sort(renumbers, TSortByOldNumber());
        assert(renumbers.size() == edges.size() + 1);

#ifdef DEBUG
        std::cerr << "Vertex Renumbering:\n";
        for (auto it : renumbers) {
            std::cerr << it.OldNumber << " => " << it.NewNumber << '\n';
        }
#endif

        JoinNewNumbers(&values, renumbers);
        Sort(values, TSortByNewVertex());

        double ans = RunTimeForwardProcessing<TVector, TPriorityQueue>(values);
        return ans;
    }

private:
    TVector<TEdge> edges;
    TVector<TEdge> backEdges;
    TVector<TValues> values;
};

typedef TSolver<TStdVector, TStdPriorityQueue> TRamSolver;
typedef TSolver<TStxxlVector, TStxxlPriorityQueue> TEmSolver;

void ReadFromTextFile(const char* name, ISolver* solver) {
    TClock clk("Read from text file");
    std::ifstream fin("input.txt");
    i64 vertex;
    i64 parent;
    i32 value;
    while (fin >> vertex >> parent >> value) {
        solver->Add(vertex, parent, value);
    }
}

int main() {
    TEmSolver solver;
    ReadFromTextFile("input.txt", &solver);

    const double ans = solver.Solve();
    std::cout << std::fixed << std::setprecision(9) << ans << '\n';
}
