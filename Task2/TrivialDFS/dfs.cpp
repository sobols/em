#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <utility>
#include <iomanip>
#include <stdexcept>

typedef long long i64;
typedef int i32;

std::map<i64, i64> values;
std::map<i64, std::vector<i64>> edges;

double DFS(i64 v) {
    const auto it = edges.find(v);
    if (it == edges.end()) {
        // leaf
        const auto jt = values.find(v);
        if (jt == values.end()) {
            throw std::runtime_error("no value for leaf");
        }
        return static_cast<double>(jt->second);
    } else {
        // not leaf
        double sum = 0;
        for (i64 w : it->second) {
            sum += DFS(w);
        }
        return sum / it->second.size();
    }
}

int main() {
    std::ifstream fin("input.txt");

    i64 vertex;
    i64 parent;
    i32 value;
    while (fin >> vertex >> parent >> value) {
        values[vertex] = value;
        edges[parent].push_back(vertex);
    }

    double ans = DFS(0);
    std::cout << std::fixed << std::setprecision(9) << ans << '\n';
}
